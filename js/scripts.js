$(document).ready(function() {
	function initFiveMinutesCountdown(){
		var time = new Date().getTime();
		var countDownDate = time + 300000;
		var c_minutes = $('.minutes');
		var c_seconds = $('.seconds');
		var countdown = setInterval(function() {
			var now = new Date().getTime();
			var distance = countDownDate - now;
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			if (Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)) > 0) {
				c_minutes.html(minutes + ':');
				c_seconds.html(seconds);
			} else {
				c_minutes.html('Время истекло');
				c_seconds.html('');
			}
		}, 1000);
	};
	initFiveMinutesCountdown()
	/*
	
	init initFiveMinutesCountdown() when needed
	
	insert the following html code, where it has to appear
	<span class="minutes"></span><span class="seconds"></span>
	
	*/
	var dayLeft = new Date("September 12, 2018 17:00:00");
	if ($('#defaultCountdown').length) {
		$('#defaultCountdown').countdown({until: dayLeft, layout: '<span class="siItem dd"><span class="numWrap"><span class="num"><span class="numInside">{dnn}</span></span></span><span class="siDesc">Дней</span></span> <span class="siItem hh"><span class="numWrap"><span class="num"><span class="numInside">{hnn}</span></span></span><span class="siDesc">Часов</span></span> <span class="siItem mm"><span class="numWrap"><span class="num"><span class="numInside">{mnn}</span></span></span><span class="siDesc">Минут</span></span>'});
	}

});

var disableScroll = false;
var scrollPos = 0;
function stopScroll() {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
}
function enableScroll() {
    disableScroll = false;
}

jQuery(function($) {

	$('.popupNavTrigger').on('click', function(){
		$(this).toggleClass('is-active');

		ps1 = new PerfectScrollbar('._scrollable', {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});

    $(window).bind('scroll', function(){
         if(disableScroll) $(window).scrollTop(scrollPos);
    });
    $(window).bind('touchmove', function(){
         $(window).trigger('scroll');
    });
	var ps1,
		ps2;
	// if ($('._scrollable').length) {}
	// ps1 = new PerfectScrollbar('._scrollable', {
	//   wheelSpeed: 1,
	//   wheelPropagation: false,
	//   suppressScrollX: true
	// });
	$('._level_1 span').on('click', function(event) {
		event.stopPropagation();
		ps1 = new PerfectScrollbar('._scrollable', {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});
	$('.hasChild').on('click', function(event) {
		// $('.mPopupNav__menu').addClass('fixed');
		if (ps1) {
			ps1.destroy();
		} 
		if (ps2) {
			ps2.destroy();
		}
		$(this).find('.childItem').first().addClass('active');
	});
	$('.childItem__goBackBtn span').on('click', function(event) {
		event.stopPropagation();
		$(this).parents('.childItem').first().removeClass('active');
	});
	$('.mPopupNavScroll').each(function(index, el) {
		const ps = new PerfectScrollbar(this, {
		  wheelSpeed: 1,
		  wheelPropagation: false,
		  suppressScrollX: true
		});
	});
	$('._close').on('click', function() {
	});

	$('.mPopupNav').on('hide.bs.modal', function () {
		if (ps1) {
			ps1.destroy();
		} 
		if (ps2) {
			ps2.destroy();
		}
		$('.popupNavTrigger').removeClass('is-active');
		$('._popupNav').find('.childItem').each(function() {
			$(this).removeClass('active');
		});
	})
	$('._pass_visibility_trig').on('click', function () {
		var pass = $(this).parents('.pass_field').find('#pass');
		if (pass.attr('type') == 'password') {
			pass.attr('type', 'text');
		} else {
			pass.attr('type', 'password');
		}
		$(this).find('.pas_icon_trig').toggle();
	})

	if ($(".date-range").length) {
		$.fn.datepicker.dates['ru'] = {
		    days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
		    daysShort: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
		    daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
		    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		    monthsShort: ["Яна", "Фев", "Мар", "Апр", "Май", "Июнь", "Июль", "Авг", "Сен", "Окт", "Ноя", "Дек"],
		    today: "Сегодня",
		    clear: "Очистить",
		    format: "dd.mm.yyyy",
		    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
		    weekStart: 1
		};
		$(".date-range").datepicker({
			inputs: $('.date-pick'),
			format: 'dd.mm.yyyy',
			language: 'ru'
		});
	}
});